<?php

namespace Adhams\LaravelJWT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;

use \Firebase\JWT\JWT;
use Carbon\Carbon;

class AuthController extends Controller
{
	private $key;
	private $issuer;

	public function __construct()
	{
		$this->key = getenv('JWT_KEY');
		$this->issuer = getenv('JWT_ISSUER');
	}
	
    public function login(Request $request)
    {
    	$credentials = $request->only(['email', 'password']);

    	$validator = Validator::make($credentials, [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()) {
            throw new ValidationHttpException($validator->errors()->all());
        }

		if( !Auth::once($credentials) )
			return response('Unauthorized.', 401);

		$user = Auth::user();

    	$token = array(
	    "iss" => $this->issuer,
	    "iat" => strtotime(Carbon::now()),
	    "exp" => strtotime(Carbon::now()->addHour()),
	    "sub" => $user->id,
		);

		$jwt = JWT::encode($token, $this->key);

		return response('')->header('Authorization', 'Bearer ' . $jwt);;
    }
}