<?php

namespace Adhams\LaravelJWT;

use Illuminate\Support\ServiceProvider;

class LaravelJWTServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Adhams\LaravelJWT\AuthController');
    }
}
